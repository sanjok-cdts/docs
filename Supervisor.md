## Installing Supervisor on AWS Elastic Beanstalk manually and automatically



## Server Information

```
$ cat /etc/os-release
NAME="Amazon Linux"
VERSION="2"
ID="amzn"
ID_LIKE="centos rhel fedora"
VERSION_ID="2"
PRETTY_NAME="Amazon Linux 2"
ANSI_COLOR="0;33"
CPE_NAME="cpe:2.3:o:amazon:amazon_linux:2"
HOME_URL="https://amazonlinux.com/"
```

**centos rhel fedora**

**nginx, php**



## Procedure

1. install supervisor

2. Create directory for supervisor workers > `mkdir /etc/supervisord.d/conf.d/`

3. Create laravel worker file > `touch /etc/supervisor/conf.d/laravel-worker.conf`

4. customize supervisord.conf

   1.  change last line. all supervisor config goes to /etc/supervisor/conf.d :: [include]
      files = /etc/supervisor/conf.d/*.conf

5. sudo service supervisord restart

6. **Start** laravel-worker and any other workers as  > sudo service supervisorctl start laravel-worker:*

7. **Check status** whether workers are running as > sudo supervisorctl status  you show see similar output as below

8. ```
   laravel-worker:laravel-worker_00   RUNNING   pid 10039, uptime 0:20:35
   laravel-worker:laravel-worker_01   RUNNING   pid 10040, uptime 0:20:35
   laravel-worker:laravel-worker_02   RUNNING   pid 10041, uptime 0:20:35
   laravel-worker:laravel-worker_03   RUNNING   pid 10042, uptime 0:20:35
   laravel-worker:laravel-worker_04   RUNNING   pid 10043, uptime 0:20:35
   ```

**Done!**



### 1. Install supervisor

***epel-release is available in Amazon Linux Extra topic "epel"***

```
$sudo amazon-linux-extras install epel

$sudo yum install supervisor

sudo systemctl enable supervisord.service

sudo systemctl start supervisord.service
```

### 2. Customize supervisord.conf


```
[unix_http_server]
file=/var/run/supervisor/supervisor.sock   ; (the path to the socket file)

[supervisord]
logfile=/tmp/supervisord.log ; (main log file;default $CWD/supervisord.log)
logfile_maxbytes=50MB        ; (max main logfile bytes b4 rotation;default 50MB)
logfile_backups=10           ; (num of main logfile rotation backups;default 10)
loglevel=info                ; (log level;default info; others: debug,warn,trace)
pidfile=//var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
nodaemon=false               ; (start in foreground if true;default false)
minfds=1024                  ; (min. avail startup file descriptors;default 1024)
minprocs=200                 ; (min. avail process descriptors;default 200)
environment=SYMFONY_ENV=prod

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl=unix:///var/run/supervisor/supervisor.sock ; use a unix:// URL  for a unix socket

[include]
files = /etc/supervisor/conf.d/*.conf
```



### 3.create laravel-websocket.conf

```
[program:laravel-websocket]
process_name=%(program_name)s_%(process_num)02d
command=php /var/www/html/artisan websocket:serve
autostart=true
autorestart=true
user=root
numprocs=5
redirect_stderr=true
stderr_logfile=/var/www/html/storage/websocket.err.log
stdout_logfile=/var/www/html/storage/websocket.log
```



### 4. create laravel-worker.conf 

```
[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /var/www/html/artisan queue:work --tries=3
autostart=true
autorestart=true
user=root
numprocs=5
redirect_stderr=true
stderr_logfile=/var/www/html/storage/worker.err.log
stdout_logfile=/var/www/html/storage/worker.log
```



.env setting of laravel for websocket and queue

```
BROADCAST_DRIVER=pusher
CACHE_DRIVER=file
QUEUE_CONNECTION=database
SESSION_DRIVER=database
SESSION_LIFETIME=120
QUEUE_DRIVER=database
```

create install_supervisor.sh and put it inside .platform/hooks/postdeploy/install_supervisor.sh

![image-20210309164119743](C:\Users\Sushan\Downloads\image-20210309164119743.png)



```
#!/usr/bin/env bash

    if [ ! -f /etc/supervisord.d ]; then
        echo "install supervisor and create directories"
        yum install supervisor
    else
        echo "supervisor already installed"
    fi

    if [ ! -d /etc/supervisor.d ]; then
        mkdir /etc/supervisor.d
        echo "create supervisor directory"
    fi

    if [ ! -d /etc/supervisor.d/conf.d ]; then
        mkdir /etc/supervisor.d/conf.d
        echo "create supervisor configs directory"
    fi
```



# to run inside supervisord
#### 1. sudo service supervisord

then run commands: 
**#Check to see if supervisor finds any new config files.**

```
reread
```

***it didn't find any config files if /etc/supervisor/conf.d/ config directory is empty.***
restart, status, reread, update

*it didn't find any config files if /etc/supervisor/conf.d/ config directory is empty.*
**restart, status, reread, update**



**The syntax is as follows for CentOS/RHEL 6.x and older (pre systemd systems):**

```
# Start the service
sudo service supervisord start

# Stop the service
sudo service supervisord stop

# Restart the service
sudo service supervisord restart
```

```
service --status-allservice --status-all 

moreservice --status-all 

grep ntpdservice --status-all | less`
```

![image-20210309111610650](C:\Users\Sushan\Downloads\image-20210309111610650.png)

[**some more useful syntax]: https://www.cyberciti.biz/faq/check-running-services-in-rhel-redhat-fedora-centoslinux/

**Type systemctl command without any options to show both loaded and active units/services:**

```
$ sudo systemctl`
```

**Filter out using the [grep command](https://www.cyberciti.biz/faq/howto-use-grep-command-in-linux-unix/)/[egrep command](https://www.cyberciti.biz/faq/grep-regular-expressions/):**

```
`$ sudo systemct | egerep 'httpd|php-fpm|mysqld|nginx'$ sudo systemctl list-units --type=service | grep nginx`
```



# Troubleshoot

**error:**  unix-tmp-supervisor-sock-no-such-file

**solution:**  sudo service supervisord restart



## Some important references

1. https://gist.github.com/israelalagbe/1696bcf61ee1686b2a93bf34d2ecfab0 (Install and use Supervisord with AWS Elastic Beanstalk, Symfony 2 and RabbitMq Bundle)
2. https://gist.github.com/vrajroham/6565c4b2e9b4db693c1524394545a610 (Installing Supervisor on AWS Elastic Beanstalk manually.)
3. [laravel-aws-eb](https://github.com/rennokki/laravel-aws-eb)
4. https://tn710617.github.io/supervisor/ (Deploy Supervisor on AWS and MacOS)
